const mongoose = require("mongoose");

const userSchema = new mongoose.Schema({
	email : {
		type : String,
		required : [true, "email is required"]
	},
	password : {
		type : String,
		required : [true, "password is required"]
	},
	isAdmin : {
		type : Boolean,
		default : false
	},
	orders : [
			{
				products : [
					{
						productName : {
							type : String,
							required : [true, "Product Name is required"]
						},
						quantity : {
							type: Number,
							required : [true, "Quantity is required"]
						}
					}
				],
				totalAmount : {
					type: Number,
					//required : [true, "totalAmount is required"]
	
				},
				purchasesOn : {
							type : Date,
							default : new Date()
						}
			}
	]
	
	
});

// userSchema.aggregate(
//     [
//       {
//         $group: {
//           _id: "$orders",
//           totalAmount: {
//             $sum: "$price"
//           }
//         }
//       }
//     ],
//     function(err, result) {
//       if (err) {
//         res.send(err);
//       } else {
//         res.json(result);
//       }
//     }
//   );
 


module.exports = mongoose.model("User", userSchema);