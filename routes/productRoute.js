const express = require("express");
const router = express.Router();

const productController = require("../controllers/productController");

const auth = require("../auth");



router.post("/create", auth.verify, (req, res) => {

	const data = {
		product: req.body,
		isAdmin: auth.decode(req.headers.authorization).isAdmin
	}
	productController.addProduct(data).then(resultFromController => res.send(resultFromController));
});



// Route for retrieving all the products
router.get("/all" , (req, res) => {
	productController.getAllProducts().then(resultFromController => res.send(resultFromController));
});

// Route for retrieving active courses
router.get("/active" , (req, res) => {
	productController.getAllActive().then(resultFromController => res.send(resultFromController));
});

// Route for retrieving specific course
router.get("/:productId", (req, res) => {
	productController.getProduct(req.params).then(resultFromController => res.send(resultFromController));
});


// Route for updating a course
router.put("/update/:productId", auth.verify, (req,res) => {
	const data = {
		product : req.body,
		productId: req.params.productId,
		isAdmin: auth.decode(req.headers.authorization).isAdmin
	}
	productController.updateProduct(data).then(resultFromController => res.send(resultFromController));
});


router.put("/archive/:productId", auth.verify, (req,res) => {
	const data = {
		product : req.body,
		productId: req.params.productId,
		isAdmin: auth.decode(req.headers.authorization).isAdmin
	}
	productController.updateArchiveProduct(data).then(resultFromController => res.send(resultFromController));
});






module.exports = router;


