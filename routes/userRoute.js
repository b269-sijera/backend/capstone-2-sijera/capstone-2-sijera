const express = require("express");


const router = express.Router();


const userController = require("../controllers/userController");

const auth = require("../auth");

// Route for checking if the user's email already exists in the database
router.post("/checkEmail", (req,res) => {
	userController.checkEmailExists(req.body).then(resultFromController => res.send(resultFromController));
})

// Route for user registration
router.post("/register", (req, res) => {
	userController.registerUser(req.body).then(resultFromController => res.send(resultFromController));
});

// Route for user authentication
router.post("/login", (req, res) => {
	userController.loginUser(req.body).then(resultFromController => res.send(resultFromController));
});


// Route for authenticated user order
router.post("/order", auth.verify, (req, res) => {


	let data = {
		userId: auth.decode(req.headers.authorization).id,
		productId: req.body.productId,
		isAdmin: auth.decode(req.headers.authorization).isAdmin
	}


	
	userController.order(data).then(resultFromController => res.send(resultFromController));
});


// Route for retrieving user details


router.post("/details", auth.verify, (req, res) => {

	
	const userData = auth.decode(req.headers.authorization);


userController.getProfile({userId : userData.id}).then(resultFromController => res.send(resultFromController));
});



router.get("/details", auth.verify, (req, res) => {
	const userData = auth.decode(req.headers.authorization);
	userController.getProfile({id: userData.id}).then(resultFromController => res.send(resultFromController))
});







module.exports = router;