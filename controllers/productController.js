const Product = require("../models/Product");


module.exports.addProduct = (data) => {
	
	if (data.isAdmin) {
		
		let newProduct = new Product({
			name : data.product.name,
			description : data.product.description,
			price : data.product.price
		});
		// Saves the created object to our database
		return newProduct.save().then((product, error) => {
			// Course creation successful
			if (error) {
				return false;
			// Course creation failed
			} else {
				return true;
			};
		});
	};
	// User is not an admin
	let message = Promise.resolve('User must be ADMIN to access this!');
	return message.then((value) => {
		return {value};
	});
};


// Retrieve ALL products
module.exports.getAllProducts = () => {
	return Product.find({}).then(result => {
		return result;
	});
};

// Retrieve ACTIVE Product
module.exports.getAllActive = () => {
	return Product.find({isActive:true}).then(result => {
		return result;
	});
};


// Retrieve specific Product
module.exports.getProduct = (reqParams) => {
	return Product.findById(reqParams.productId).then(result => {
		return result;
	});
};


// Update a product
module.exports.updateProduct = (data) => {
	if (data.isAdmin) {
	let updatedProduct = {
		name: data.product.name,
 		description: data.product.description,
 		price: data.product.price
	};

	// findByIdAndUpdate(document ID, updatesTobeApplied)
	return Product.findByIdAndUpdate(data.productId, updatedProduct).then((product, error) => {
		if(error) {
			return false;
		} else {
			return true
		};
	});
  };	
	// User is not an admin
	let message = Promise.resolve('User must be ADMIN to access this!');
	return message.then((value) => {
		return {value};
	});
};


// Archive a course
module.exports.updateArchiveProduct = (data) => {
	if (data.isAdmin) {
	let archivedProduct = {
		isActive: data.product.isActive
	};

	// findByIdAndUpdate(document ID, updatesTobeApplied)
	return Product.findByIdAndUpdate(data.productId, archivedProduct).then((product, error) => {
		if(error) {
			return false;
		} else {
			return true
		};
	});
  };	
	// User is not an admin
	let message = Promise.resolve('User must be ADMIN to access this!');
	return message.then((value) => {
		return {value};
	});
};

	



