const User = require("../models/User");
const Product = require("../models/Product")
const mongoose = require("mongoose");



const bcrypt = require("bcrypt");

const auth = require("../auth");


// Checks email if already exist
module.exports.checkEmailExists = (reqBody) => {
	return User.find({email : reqBody.email}).then(result => {
		if (result.length > 0) {
			return true;
		} else {
			return false;
		};
	});
};


// User register
module.exports.registerUser = (reqBody) => {
	let newUser = new User({
		firstName: reqBody.firstName,
		lastName: reqBody.lastName,
		email: reqBody.email,
		mobileNo: reqBody.mobileNo,
		password: bcrypt.hashSync(reqBody.password, 10)
	});
	return newUser.save().then((user,error) => {
		if (error) {
			return false;
		} else {
			return true;
		};
	});
};


// User Authentication
module.exports.loginUser = (reqBody) => {
	return User.findOne({email: reqBody.email}).then(result =>{
		// User does not exist
		if (result == null){
			return false
		  // User exists
		} else {
			const isPasswordCorrect = bcrypt.compareSync(reqBody.password, result.password);
			if (isPasswordCorrect) {
				return {access: auth.createAccessToken(result)}
			} else {
				return false;
			};
		};
	});
};



module.exports.order = async (data) => {
	if(data.isAdmin) {
		return Promise.resolve({value: "User must Not be an Admin to access this!"})
	};
	// Add product ID in the orders array of the user
	let isUserUpdated = await User.findById(data.userId).
		then(user => {
			user.orders.push({
				productId: data.productId});

			return user.save().then((user, error) => {
				if (error){
					return false;
				} else {
					return true;
				};
			});
	});



		// Add user ID in the orders array of the product
		let isProductUpdated = await Product.findById(data.productId).then(product => {
			product.orders.push({userId: data.userId});
			return product.save().then((product, error) => {
				if (error){
					return false;
				} else {
					return true;
				};
			});
		});

		if (isUserUpdated && isProductUpdated) {
			return true;
		} else {
			return false;
		};
}


// Retrieve User details
module.exports.getProfile = (data) => {
	return User.findById(data.userId).then(result => {
		
		result.password = "";

		return result;
	});
};


module.exports.getProfile = (userData) => {
	return User.findById(userData.id).then(result => {
		if (result == null) {
			return false
		} else {
			result.password = "*****"
			// Returns the user information with the password as an empty string or asterisk.
			return result
		}
	})
};








